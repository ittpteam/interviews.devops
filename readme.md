## Interview.DevOps
Deploy API and WEB projects using CI/CD

* Automate deployment for two different environments
	* DEV and QAS
	* Environment name should be set on appsetting.json settings:env
* Use Azure App Services
* Use Azure Pipelines
* Include UnitTest validation in pipeline
* Design a process (include tools) to deploy SQL Server Database schema and data changes within the pipeline (only process definition, no implementation needed)

### Deliverables
Send a document with following information:

* Evidence (log/screenshots) of success pipeline execution
* Repository link
* Evidence (log/screenshots) of execution in the two diferents enviroments (DEV/QAS)
* Git worlflow
* Database CI/CD process definition

_Be prepared to **show a live execution** during the interview_